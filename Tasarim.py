import time


class Tsrm ():
    
    def __init__(self):
        self.YataySol()
        self.Dikey()
        self.YataySol()
        
    def YataySag(self):
        yataysag = ["__"," __","  __","   __","    _","      _"]       
        for ysg in range(1):
            for ytysg in yataysag:
                print (5*ytysg,end = "\r")
                time.sleep(0.2)
                
    def Dikey(self):
        dikey = ["\n|\t\t\t\t\t |","|\t\t\t\t\t |","|\t\t\t\t\t |","|\t\t<Coded By>\t\t |","|\t\t ~|MSF4|~\t\t |","|\t\t\t\t\t |","|\t\t\t\t\t |"]
        for d in range(1):
            for dky in dikey:
                print (dky,end = "\n")
                time.sleep(0.2)
                
    def YataySol(self):
        yataysol = ["\t\t\t\t  _","\t\t\t   _","\t\t    _","\t     _","      _"]
        for ysl in range(1):
            for ytysl in yataysol:
                print (ytysl, end = "\r")
                time.sleep(0.2)
                


app = Tsrm()
